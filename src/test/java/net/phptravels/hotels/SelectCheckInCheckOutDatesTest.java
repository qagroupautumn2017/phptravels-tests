package net.phptravels.hotels;

import java.time.LocalDate;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.PhpTravelsApp;
import com.qagroup.phptravels.ui.DatePicker;

public class SelectCheckInCheckOutDatesTest {

	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	private MainPage mainPage;
	private DatePicker datePicker;

	@Test
	public void testNavigationToHotels() {
		mainPage = phpTravelsApp.openMainPage();
		datePicker = mainPage.openHotelsCheckInDatePicker();
		datePicker.selectDate(LocalDate.now().plusYears(2));
		datePicker.selectDate(LocalDate.now().plusYears(2).plusWeeks(1));
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

}
