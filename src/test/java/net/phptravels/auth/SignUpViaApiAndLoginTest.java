package net.phptravels.auth;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.AccountPage;
import com.qagroup.phptravels.LoginPage;
import com.qagroup.phptravels.SignUpApi;
import com.qagroup.phptravels.User;
import com.qagroup.tools.CustomAsserts;

import net.phptravels.tests.AbstractTest;

public class SignUpViaApiAndLoginTest extends AbstractTest {

	private User user;
	private LoginPage loginPage;
	private AccountPage accountPage;

	@BeforeMethod
	public void setup() {
		user = User.generate();
		SignUpApi.postSignUp(user);
		loginPage = phpTravelsApp.openLoginPage();
	}

	@Test
	public void testLogin() {
		accountPage = loginPage.loginAs(user.getEmail(), user.getPassword());

		String actualUrl = accountPage.getCurrentUrl();
		String expectedUrl = "http://www.phptravels.net/account/";

		Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");

		String actualUserNameOnHeader = accountPage.readUserName();
		CustomAsserts.assertEquals(actualUserNameOnHeader, user.getFirstName().toUpperCase(),
				"Username on header should be equal to expected.");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}
}
