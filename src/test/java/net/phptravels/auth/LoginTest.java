package net.phptravels.auth;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.AccountPage;
import com.qagroup.phptravels.LoginPage;
import com.qagroup.tools.CustomAsserts;

import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import net.phptravels.tests.AbstractTest;

//@Epic("Authentication")
@Feature("Authentication")
@Story("Login")
public class LoginTest extends AbstractTest {

	private LoginPage loginPage;

	private AccountPage accountPage;

	@Issue("IN-123")
	@TmsLink("TC-456")
	@Link("https://driver.google.com")
	@Test(groups = "Auth")
	public void testLogin() {
		loginPage = phpTravelsApp.openLoginPage();
		accountPage = loginPage.loginAs("user@phptravels.com", "demouser");

		String actualUrl = accountPage.getCurrentUrl();
		String expectedUrl = "http://www.phptravels.net/account/";

		Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");

		String actualUserNameOnHeader = accountPage.readUserName();
		CustomAsserts.assertEquals(actualUserNameOnHeader, "JOHNY", "Username on header should be equal to expected.");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

}
