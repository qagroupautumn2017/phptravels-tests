package com.qagroup.phptravels;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qagroup.tools.WebElementUtils;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class AccountPage extends BasePhpTravelsPage {

	@FindBy(css = ".currency_btn>.sidebar>li:nth-of-type(1)>a.dropdown-toggle.go-text-right")
	private WebElement usernameOnHeader;

	private WebDriver driver;

	public AccountPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		waitUntilLoaded();
	}

	private void waitUntilLoaded() {
		WebDriverWait wait = new WebDriverWait(this.driver, 10);
		wait.until(presenceOfElementLocated(By.cssSelector("#body-section img.img-thumbnail")));
	}

	@Step("Read current URL")
	@Attachment("URL")
	public String getCurrentUrl() {
		return this.driver.getCurrentUrl();
	}

	@Step("Read username from header")
	@Attachment("Username")
	public String readUserName() {
		return WebElementUtils.getText(usernameOnHeader);
	}

}
