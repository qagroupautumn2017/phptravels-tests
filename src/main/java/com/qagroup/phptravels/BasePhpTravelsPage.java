package com.qagroup.phptravels;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qagroup.phptravels.ui.DatePicker;
import com.qagroup.tools.AbstractWebPage;

import io.qameta.allure.Step;

public class BasePhpTravelsPage extends AbstractWebPage {

	@FindBy(css = "nav.navbar-orange.navbar-default")
	private WebElement navigationBar;

	@FindBy(css = ".datepicker[style*='display: block']")
	private WebElement datePicker;

	@FindBy(css = "[name='checkin']")
	private WebElement checkInInput;

	private WebDriver driver;

	public BasePhpTravelsPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	public NavigationBar navigationBar() {
		return new NavigationBar(navigationBar);
	}

	public DatePicker datePicker() {
		return new DatePicker(datePicker);
	}

	@Step("Open 'Check In' date picker")
	public DatePicker openHotelsCheckInDatePicker() {
		checkInInput.click();
		DatePicker picker = datePicker();
		return picker;
	}

}
