package com.qagroup.phptravels;

import com.github.javafaker.Faker;

public class User {

	private final String firstName;

	private final String lastName;

	private final String mobileNumber;

	private final String email;

	private final String password;

	public User(String firstName, String lastName, String mobileNumber, String email, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.password = password;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", mobileNumber=" + mobileNumber + ", email="
				+ email + ", password=" + password + "]";
	}

	public static User generate() {
		Faker faker = new Faker();
		String fName = faker.name().firstName();
		String lName = faker.name().lastName();
		String mNum = faker.phoneNumber().cellPhone();
		String em = faker.internet().emailAddress();
		String pass = faker.internet().password();
		return new User(fName, lName, mNum, em, pass);
	}

	public static void main(String[] args) {
		for (int i = 1; i < 5; i++)
			System.out.println(generate());
	}

}
