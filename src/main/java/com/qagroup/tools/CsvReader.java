package com.qagroup.tools;

import java.io.FileReader;
import java.io.Reader;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class CsvReader {

	public static List<CSVRecord> read(String pathToCsvFile) {
		try {
			Reader in = new FileReader(pathToCsvFile);
			CSVParser parser = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
			return parser.getRecords();
		} catch (Exception e) {
			throw new RuntimeException("Couldn't read a .CSV file");
		}
	}
	
}
