package com.qagroup.tools;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

public class TestResultListener implements IResultListener2 {

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("SUCCESS!!!");

		if (result.getInstance() instanceof WebAppTest) {
			WebAppTest testObj = (WebAppTest) result.getInstance();
			testObj.getTestedApp().takeScreenshot("Success screenshot");
		}

	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("FAILURE!!!");
		if (result.getInstance() instanceof WebAppTest) {
			WebAppTest testObj = (WebAppTest) result.getInstance();
			testObj.getTestedApp().takeScreenshot("Failure screenshot");
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("SKIPPED!!!");
		if (result.getInstance() instanceof WebAppTest) {
			WebAppTest testObj = (WebAppTest) result.getInstance();
			testObj.getTestedApp().takeScreenshot("Skipped screenshot");
		}
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationSuccess(ITestResult itr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationFailure(ITestResult itr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationSkip(ITestResult itr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeConfiguration(ITestResult tr) {
		// TODO Auto-generated method stub

	}

}
