package com.qagroup.tools;

public interface WebApp {
	public byte[] takeScreenshot(String screenshotName);
}
